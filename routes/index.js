var express = require('express');
var router = express.Router();
const cheerio = require("cheerio");
const request = require("request");

/* GET home page. */
router.get('/', function (req, res, next) {
    res.render('list', {title: 'Express'});
});

router.get('/genre/:genre', function (req, res, next) {
    let genre = req.params.genre;
    res.render('list', {genre:genre});
});


function doit(req, res,url) {
    request(url, function (err, response, html) {
        if (err) {
            console.log(err);
            return res.status(500).json(err);
        }

        let $ = cheerio.load(html);

        let arr = [];
        $("article.box").each(function () {
            let film = {};
            film.title = $(this).find('.title').text().trim();

            if (film.title.indexOf("سریال") < 0 /* && film.title.indexOf("مراسم") < 0*/) {
                film.img = $(this).find('#myimg').attr('src');
                film.imdb = $(this).find("span[style*='color: #ffffff;']").text().trim();
                film.story = $(this).find("div[style*='background-color: #f3f3f3; width: 508px; text-align: right; margin-top: 1px;']").text().trim();
                let info = $(this).find("div[style*='background: #f3f3f3; height: 305px; width: 508px; text-align: right;'] > p").text().trim();
                film.duration = getbetween(info, "مدت زمان :", "زبان");
                film.language = getbetween(info, "زبان :", "کیفیت");
                film.quality = getbetween(info, "کیفیت :", "فرمت");
                film.format = getbetween(info, "فرمت : ", "انکودر");
                film.encoder = getbetween(info, "انکودر :", "حجم");
                film.size = getbetween(info, "حجم :", "محصول");
                film.country = getbetween(info, "محصول :", "ستارگان");
                film.stars = getbetween(info, "ستارگان : ", "کارگردان");
                film.director = getbetween(info, "کارگردان : ", "لینک");

                film.link = $(this).find('.title a').attr('href');

                if (film.title.indexOf("دانلود فیلم"> 0)){
                    film.title= film.title.replace("دانلود فیلم" , "").trim();
                }

                console.log(film.title);

                film.title = film.title.replace("دانلود مراسم","").trim();


                //get genre
                let genre  = $(this).find("span[style*='color: #fa5705;']").first().text().split(",");
                for (let i = 0; i < genre.length; i++) {
                    genre[i] = genre[i].trim();
                }

                film.imdb_link = $(this).find("a[href*='imdb']").attr('href');

                film.genre = genre;


                arr.push(film);
            }


        });

        res.json(arr);

    })
}

router.get('/api/film/:page', function (req, res, next) {
    let url = "https://www.film2movie.ws/page/" + req.params['page'];
    doit(req,res,url);
});


router.get('/api/film/:genre/:page', function (req, res, next) {
    let url = `https://www.film2movie.ws/category/download-film/film-janre/${req.params['genre']}/page/${req.params['page']}/`;
    doit(req,res,url);
});





function getbetween(data, from, to) {
    return data.slice(data.indexOf(from) + from.length, data.indexOf(to)).trim();
}


module.exports = router;
